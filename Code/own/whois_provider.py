import abc
import string

from constants import GENERIC_FIELDS, MAPPINGS
from pywhois.whois import whois
from exceptions import IncorrectFormatError


class WhoisProvider(metaclass=abc.ABCMeta):
    @staticmethod
    @abc.abstractmethod
    def parse(domain):
        raise NotImplementedError()

    @abc.abstractmethod
    def convert_to_csv(self):
        raise NotImplementedError()


class WhoisData(WhoisProvider):
    def __init__(self, given_fields):
        try:
            self._tld = self._get_tld(given_fields)
        except (KeyError, AttributeError):
            raise IncorrectFormatError("Couldn't get the TLD")

        mapping = MAPPINGS.get(self._tld, {})

        for field in given_fields:
            new_field_name = mapping[field] if (field in mapping) else field
            setattr(self, new_field_name, given_fields[field])

        if hasattr(self, 'name_servers'):
            self._fix_name_servers()

        for field in GENERIC_FIELDS:
            if not hasattr(self, field):
                setattr(self, field, None)

    def convert_to_csv(self):
        keys = ','.join(GENERIC_FIELDS)
        cleaned_values = [self._clean_string(getattr(self, field))
                          for field in GENERIC_FIELDS]
        values = ','.join([f"\'{value}\'" for value in cleaned_values])
        return keys, values

    def _fix_name_servers(self):
        servers = getattr(self, 'name_servers')
        if type(servers) == str:
            servers = [s.strip() for s in servers.split('\n')]
            if servers:
                setattr(self, 'name_servers', f"({','.join(servers)})")

    @staticmethod
    def parse(domain):
        raw_data = whois(domain)
        return WhoisData(raw_data)

    @staticmethod
    def _get_tld(fields):
        possible_values = ['domain_name', 'domain']
        for value in possible_values:
            if value in fields:
                tld = fields[value][0] if type(fields[value]) == list else fields[value]
                tld = tld.split('.')[-1].lower()
                return WhoisData._remove_non_string_characters(tld)

    @staticmethod
    def _remove_non_string_characters(tld):
        def letter(x):
            return x in string.ascii_letters
        return ''.join(filter(letter, tld))

    @staticmethod
    def _clean_string(s):
        return str(s).replace("'", "") if s else ''

    def __repr__(self):
        base = f"<Whois:"
        for field in GENERIC_FIELDS:
            base += f" {field}={getattr(self, field)}."
        return base + ">"
