GENERIC_FIELDS = ['domain_name', 'status', 'registrant', 'registrant_address', 'admin_name',
                  'admin_address', 'tech_name', 'tech_address', 'phone', 'email']
MAPPINGS = {
    'it': {'registrant_organization': 'registrant'},
    'fi': {'address': 'registrant_address', 'name': 'registrant'},
    'br': {'domain': 'domain_name', 'person': 'registrant', 'e-mail': 'email'},
    'ar': {'domain': 'domain_name', 'name': 'registrant'},
    'au': {'registrant_contact_name': 'registrant'},
    'info': {'emails': 'email', 'address': 'registrant_address'},
    'biz': {'registrant_name': 'registrant', 'registrant_phone_number': 'phone',
            'registrant_email': 'email'},
    'mobi': {'registrant_name': 'registrant', 'registrant_phone_number': 'phone',
             'registrant_email': 'email'},
    'org': {'emails': 'email', 'address': 'registrant_address'},
    'ca': {'emails': 'email', 'registrant_name': 'registrant'},
    'at': {'name': 'registrant', 'address': 'registrant_address'},
    'us': {'registrant_email': 'email'}
}
