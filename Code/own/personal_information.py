import sqlite3


def parse_whois_data(db_name='../data/malicious_domains_crawling.sqlite3',
                     table_name='italian_whois'):
    conn = sqlite3.connect(db_name)
    cursor = conn.cursor()
    rows = _get_rows(cursor, table_name)
    return rows


def _get_rows(cursor, table_name):
    query = f'SELECT * FROM {table_name}'
    cursor.execute(query)
    return cursor.fetchall()


def determine_personal_information(data):
    tld = _get_tld(data.domain_name)
    if tld == '.nl':
        _determine_nl(data)
    elif tld == '.it':
        _determine_it(data)


def _determine_nl(data):
    registrar = data['registrar']
    return registrar == 'Henk'


def _determine_it(data):
    pass


def _get_tld(domain_name):
    return domain_name.split('.')[-1]


if __name__ == '__main__':
    parse_whois_data()
