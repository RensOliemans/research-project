import sqlite3
import pandas as pd
from loguru import logger

import constants

MINIMUM_TLDS = 10
FILTERED_TLDS = {'US]': 'us2'}


def get_data(db_name, table):
    c = sqlite3.connect(db_name)
    cur = c.cursor()

    cur.execute(f"SELECT * FROM {table}")
    data = cur.fetchall()

    c.close()
    return data


def determine_tld(entry):
    try:
        domain = entry[0]
        return domain.split('.')[-1]
    except (KeyError, AttributeError):
        logger.warning("Finding the tld of {} resulted in an error", entry)
        return 'unknown'


def get_tlds(data):
    tlds = list(set([determine_tld(entry) for entry in data]))
    tlds = [tld for tld in tlds
            if len([entry for entry in data
                    if determine_tld(entry) == tld]) >= MINIMUM_TLDS]

    return tlds


def rewrite_tld(tld):
    if tld in FILTERED_TLDS:
        return FILTERED_TLDS[tld]
    return tld


def output_to_xls(data, filename, fields):
    writer = pd.ExcelWriter(filename)

    tlds = get_tlds(data)
    logger.debug("Got the following tlds: {}", tlds)

    for tld in tlds:
        relevant_data = [entry for entry in data if tld == determine_tld(entry)]
        df = pd.DataFrame({field: [entry[index] for entry in relevant_data]
                           for index, field in enumerate(fields)})
        tld = rewrite_tld(tld)
        df.to_excel(writer, tld, index=False)
        logger.debug("Written to excel file with datasheet {}", tld)
        writer.save()

    writer.close()


def convert_to_sqlite3(db_name='../data/domainnames.sqlite3', input_table='whois',
                       fields=constants.GENERIC_FIELDS, output_filename='Output.xls'):
    data = get_data(db_name, input_table)
    logger.debug("Got {} data entries", len(data))

    output_to_xls(data, output_filename, fields)


if __name__ == '__main__':
    convert_to_sqlite3()
