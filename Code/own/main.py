from db_provider import SqliteHandler
from whois_db_handler import WhoisDatabaseHandler


def _setup_socks():
    import os
    os.environ['SOCKS'] = '127.0.0.1:8080'


if __name__ == '__main__':
    # _setup_socks()

    with SqliteHandler(db_name='../data/test.sqlite3', output_table_name='whois',
                       top_level_domain='.us') as sql_handler:
        handler = WhoisDatabaseHandler(sql_handler)
        handler.main()
