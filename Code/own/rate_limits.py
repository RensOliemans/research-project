import socket

from loguru import logger

from own.main import SqliteHandler, WhoisData

TLDS = ['.nl', '.it', '.org', '.ar']
MAX_ATTEMPT = 5


def get_domains(handler, tld):
    handler.change_tld(tld)
    rows = handler.get_rows(filtered=False)
    return [row[0] for row in rows]


class RateLimitError(Exception):
    pass


class InvalidDataError(Exception):
    pass


def _get_info(domain):
    try:
        data = _get_data(domain)
        logger.debug("No timeout for domain {}. data: {}", domain, data)
    except (socket.timeout, InvalidDataError) as e:
        raise RateLimitError(e)


def _get_data(domain):
    parsed_object = WhoisData.parse(domain)
    if not parsed_object.is_valid:
        raise InvalidDataError()
    return parsed_object


def find_rate_limit(domain_names):
    tld = _get_tld(domain_names[0])
    successive_correct_attempts = 0

    for domain in domain_names:
        try:
            _get_info(domain)
        except RateLimitError:
            logger.info("Found rate limit for tld .{}", tld)
            return successive_correct_attempts
        else:
            successive_correct_attempts += 1


def _get_tld(domain_name):
    return domain_name.split('.')[-1]


def main(tlds):
    handler = SqliteHandler()
    domains = dict()
    for tld in tlds:
        domains[tld] = get_domains(handler, tld)
    logger.debug("Current found domains: {}", domains)

    rate_limits = dict()
    for tld in domains.keys():
        rate_limits[tld] = find_rate_limit(domains[tld])
    logger.debug("Rate limits: {}", rate_limits)

    raise NotImplementedError()
    # pretty_print_rate_limits(rate_limits)


if __name__ == '__main__':
    main(['.it'])
