import socket
import time

from loguru import logger

from exceptions import IncorrectFormatError
from pywhois.whois.parser import PywhoisError
from whois_provider import WhoisData


class WhoisDatabaseHandler:
    def __init__(self, db_handler, whois_provider=WhoisData, sleep_time=2):
        """
        Creates a DataBuilder. This gets a lot of domain names and gives them to a database_handler
        :param whois_provider: method to use to call whois
        :param sleep_time: Time to sleep in between calls, since whois is slow
        """
        self._db_handler = db_handler
        self._whois_provider = whois_provider
        self._sleep_time = sleep_time

    @logger.catch
    def main(self):
        self._fill_data()

    def _fill_data(self):
        domains = self._get_domain_names()
        logger.debug("Starting to fill data for {} domains.", len(domains))
        for index, domain in enumerate(domains):
            logger.debug("Requesting whois data from #{}: {}", index, domain)
            self._fill_domain(domain)

    def _get_domain_names(self, amount=0):
        rows = self._db_handler.get_rows(amount, filtered=False)
        return [row[0] for row in rows]

    def _fill_domain(self, domain):
        try:
            data = self._get_whois_data(domain)
            self._set_information(data)
        except (socket.timeout, PywhoisError, ConnectionError, socket.gaierror, IncorrectFormatError) as e:
            logger.warning("Got an error on domain {}. Continuing with next domain. Error: {}",
                           domain, e)
            # time.sleep(self._sleep_time)

    def _get_whois_data(self, domain):
        return self._whois_provider.parse(domain)

    def _set_information(self, whois_data):
        keys, values = whois_data.convert_to_csv()
        self._db_handler.fill_rows(keys, values)
