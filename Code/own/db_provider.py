import abc
import sqlite3
from sqlite3 import OperationalError

from loguru import logger

from constants import GENERIC_FIELDS


class DbHandler(metaclass=abc.ABCMeta):
    @abc.abstractmethod
    def get_rows(self, amount, filtered):
        raise NotImplementedError()

    @abc.abstractmethod
    def change_tld(self, tld):
        raise NotImplementedError()

    @abc.abstractmethod
    def fill_rows(self, row_names, row_values):
        raise NotImplementedError()

    @abc.abstractmethod
    def __enter__(self):
        raise NotImplementedError()

    @abc.abstractmethod
    def __exit__(self, exc_type, exc_val, exc_tb):
        raise NotImplementedError()


class SqliteHandler(DbHandler):
    DB_DIR = '../data/'
    DB_NAME = 'malicious_domains_crawling.sqlite3'
    INPUT_TABLE_NAME = 'domain_names'
    INPUT_COLUMN = 'domain'
    OUTPUT_TABLE_NAME = 'generic_whois'
    TOP_LEVEL_DOMAIN = '.nl'

    def __init__(self, db_name=DB_DIR + DB_NAME,
                 input_table=INPUT_TABLE_NAME, input_column=INPUT_COLUMN,
                 output_table_name=OUTPUT_TABLE_NAME, output_columns=None,
                 top_level_domain=TOP_LEVEL_DOMAIN):

        self._db_name = db_name
        self._input_table = input_table
        self._input_column = input_column

        self._output_table_name = output_table_name
        self._output_fields = ','.join(output_columns or GENERIC_FIELDS)

        self._tld = self._build_tld(top_level_domain)

        self._initialize_database_connection()
        self._validate_domain_table()

    def get_rows(self, amount=0, filtered=True):
        if filtered:
            return self._get_filtered_rows(amount)
        else:
            return self._get_unfiltered_rows(amount)

    def _get_filtered_rows(self, amount):
        query = (f'SELECT {self._input_table}.domain FROM {self._input_table} '
                 f'WHERE {self._input_table}.domain LIKE \'%.{self._tld}\' '
                 f'AND {self._input_table}.domain NOT IN '
                 f'(SELECT {self._output_table_name}.domain_name '
                 f'FROM {self._output_table_name})')
        return self._execute_fetch_query(query, amount)

    def _get_unfiltered_rows(self, amount=0):
        query = (f'SELECT {self._input_table}.domain FROM {self._input_table} '
                 f'WHERE {self._input_table}.domain LIKE \'%.{self._tld}\'')
        return self._execute_fetch_query(query, amount)

    def fill_rows(self, row_names, row_values):
        query = (f'INSERT INTO {self._output_table_name} ({row_names}) '
                 f'VALUES ({row_values})')
        logger.debug("Executing {}", query)
        try:
            self._cursor.execute(query)
            self._conn.commit()
        except OperationalError as e:
            logger.error("SQL Error ({}). Output database: {}. Needed output columns: {}",
                         e, self._output_table_name, self._output_fields)
            raise ValueError(f"The provided output database is of incorrect format. See: {e}")

    def change_tld(self, tld):
        self._tld = self._build_tld(tld)

    def _initialize_database_connection(self):
        self._conn = sqlite3.connect(self._db_name)
        self._cursor = self._conn.cursor()
        logger.debug("Started database connection for database with name {}", self._db_name)
        self._initialize_output_table()

    def _validate_domain_table(self):
        try:
            self._cursor.execute(f"SELECT * FROM {self._input_table}")
            self._cursor.fetchall()
        except OperationalError:
            raise ValueError(f"A table with name {self._input_table} does not exist yet.")

    def _initialize_output_table(self):
        self._cursor.execute(f'CREATE TABLE IF NOT EXISTS {self._output_table_name}'
                             f'({self._output_fields})')

    def _execute_fetch_query(self, query, amount):
        logger.debug("Executing {}", query)
        self._cursor.execute(query)
        return self._cursor.fetchmany(amount)

    @staticmethod
    def _build_tld(top_level_domain):
        return top_level_domain.lstrip('%').lstrip('.')

    def __enter__(self):
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        logger.debug("Stopping database connection for database {}", self._db_name)
        self._conn.close()
