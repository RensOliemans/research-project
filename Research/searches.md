# Searches
This is an important file: it contains all searches done, with the (possible)
research papers found with that search


## General
* https://lirias.kuleuven.be/2123581?limo=0
* https://www.ingentaconnect.com/content/hsp/jcs/2018/00000002/00000002/art00006
* https://www.researchgate.net/profile/Tanja_Porcnik2/publication/330194322_Future_of_Europe_Security_and_Privacy_in_Cyberspace/links/5c3334ac299bf12be3b4cdcf/Future-of-Europe-Security-and-Privacy-in-Cyberspace.pdf#page=54
* https://heinonline.org/HOL/LandingPage?handle=hein.journals/thetmr108&div=49&id=&page=&t=1556786829

## Subject-specific

### WHOIS
* https://en.wikipedia.org/wiki/WHOIS
* https://whois.icann.org/en
* https://whois.icann.org/en/technical-overview
* https://www.ietf.org/rfc/rfc3912.txt

### GDPR
* https://ec.europa.eu/commission/priorities/justice-and-fundamental-rights/data-protection/2018-reform-eu-data-protection-rules_en
#### News articles concerning WHOIS 'vs' GDPR
* https://umbrella.cisco.com/blog/2018/05/31/gdpr-and-whois/
* https://www.helpnetsecurity.com/2019/04/05/whois-after-gdpr/
* https://www.forbes.com/sites/davelewis/2017/12/21/whois-the-first-casualty-of-gdpr/

### ICANN
* https://en.wikipedia.org/wiki/ICANN
* https://www.icann.org/news/blog/data-protection-privacy-update-guidance-on-proposed-model-submissions-now-available
* https://www.icann.org/dataprotectionprivacy
* https://www.icann.org/resources/pages/gtld-registration-data-specs-en
* https://www.icann.org/en/system/files/files/iana-functions-18dec15-en.pdf
* https://www.icann.org/en/system/files/files/domain-names-beginners-guide-06dec10-en.pdf
* https://www.icann.org/en/system/files/files/participating-08nov13-en.pdf
    Contains information and an easy explanation of ICANN's tasks.
