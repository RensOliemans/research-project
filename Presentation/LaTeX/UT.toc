\beamer@sectionintoc {1}{WHOIS? GDPR?}{2}{0}{1}
\beamer@sectionintoc {2}{Research Questions}{12}{0}{2}
\beamer@sectionintoc {3}{RQ 1: Parsing}{15}{0}{3}
\beamer@sectionintoc {4}{RQ 2: Personal Data}{20}{0}{4}
\beamer@sectionintoc {5}{RQ 3: Personal Data: EU and Non-EU TLDs}{26}{0}{5}
\beamer@sectionintoc {6}{RQ 4: Personal Data: Differences Within EU TLDs}{30}{0}{6}
\beamer@sectionintoc {7}{Rate Limits}{34}{0}{7}
\beamer@sectionintoc {8}{Limitations}{36}{0}{8}
\beamer@sectionintoc {9}{Conclusions}{41}{0}{9}
