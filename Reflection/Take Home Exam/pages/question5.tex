\section[Artificial Intelligence]{Question Five\\
	Artificial Intelligence}
\label{sec:ai.q5}
\subsection{Chinese Room Argument}
Searle's Chinese Room argument is a thought experiment about thinking, understanding, and intentionality.
The thought experiment is as follows\parencite{searle1980minds}:

Suppose that a man (for example, John Searle) is locked in a room with a large amount of Chinese writing. John Searle does not know any Chinese and the writing is nothing more than meaningless squiggles to him.

Searle then receives a set of rules that he can understand, and a second batch of Chinese writing. The rules explain how to correlate the different symbols of the different batches to each other, and these rules also instruct him how to give back Chinese symbols. If the rules are good enough, an outsider would be able to give Chinese questions to the room and receive perfect Chinese answers.

In fact, the outsider would not be able to distinguish John Searle from a Chinese person, in terms of speaking Chinese.
However, intuitively many would claim that Searle does not truly \textit{understand} Chinese, but merely applies the rules given to him. This leads us to the question of how much a computer's program would understand what they would do, regardless of how impressive or \textit{real} it might seem.

\subsection{Objections}
There are a couple of objections made to the Chinese Room Argument over the years. Searle replies to some of these objections in his paper\parencite{searle1980minds}, namely:

\begin{itemize}
	\item Systems Reply: The individual person in the room does not understand the story, but the entire system does.
	\item Robot Reply: The machine would understand Chinese if, next to the symbolic in-/outputs, it would also have motor outputs, and visual/audio inputs.
	\item Brain Simulator Reply: The actual sequence of neurons and synapses in the brain are simulated. If the machine doesn't understand the stories, how can we then say that native Chinese speakers understand the stories?
	\item Combination Reply: Take the previous three replies and combine them, collectively they carry much more weight.
	\item Other Minds Reply: Native Chinese speakers are judged for their understanding of Chinese by their behaviour. The same can be said for the machine.
	\item Many Mansions Reply: The current argument only works for the present state of technology. Eventually we could create devices that could have the causal processes necessary for intentionality.
\end{itemize}

\subsection{Most Convincing Objection}
In my opinion, the Other Minds reply is the best objection to the Chinese Room Argument. In order to explain why, let us first look at Searle's argument.

John Searle's argument relies on the fact that symbol manipulation could never lead to intentionality, nor true understanding of something\parencite[11]{searle1980minds}. When his paper was released (by now nearly 40 years ago), researchers believed that computation (and thinking, for a large part) were based in symbol manipulation of some sort.

However, when taking a computer with, for example, a deep learning algorithm using a neural network, it does not perform a formal algorithm, nor does it have to solve a well-defined problem. Additionally, there is no formal definition for ``understanding'', but instead we rely on an intuitive definition or sense of what understanding means. We do know that no understanding is infinite, nor is it absolute (there is a large area ``in between'' not understanding and understanding).

Not all algorithms use formal symbol manipulation to solve a problem, but some ``learn'' from good and bad examples which lead to it gradually becoming better in translating works. Or, put otherwise, it could lead to gradually understanding more Chinese.

Seeing the Room as one entity (similar to the Systems Reply) would lead to seeing the entire Room as a system which understands Chinese. In a similar sense, a cat might ``understand'' what you say when you want to feed it, but it would likely understand you \textit{a bit}.

In my opinion, John Searle's reply to the Other Minds reply insufficiently elaborates on the lack of formal symbol manipulation in AI. Furthermore, it seems to see ``understanding'' erroneously as an on-off state.