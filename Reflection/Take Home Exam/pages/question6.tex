\section[Contemporary AI]{Question Six\\
	Contemporary AI}
\subsection{Climbing to the Moon}
\subsubsection*{Criticism of Progress}
One specious definition of progress that Dreyfus particularly criticised is seeing it as ``displacement toward the ultimate goal''. Additionally, he criticised seeing every ``displacement toward the ultimate goal'' as progress.

An interesting analogy that Dreyfus makes is a man climbing a tree to get to the moon. Certainly, climbing a tree gets the man closer to the moon and according to the definition of progress explained above, this would count as progress.
No sensible person, however, would expect the man to eventually reach his ultimate goal by climbing the tree, regardless of their tree-climbing expertise.

\begin{figure}[h]
	\centering
	\includegraphics[width=.9\linewidth]{media/ManTree.png}
	\caption{Man Attempting to Climb to the Moon}
	\label{fig:manclimbstomoon}
\end{figure}

As seen in figure \ref{fig:manclimbstomoon}, a man comes a bit closer to their goal. In no way, however, will that man ever reach the moon using this method. Using this method might even delay him, since he isn't seeing the entire picture and is wasting his time and effort.

\subsubsection*{Brooks and Dennett}
Brooks and Dennett look at intelligence in a way that Dreyfus claims to be similar to the man reaching for the moon. They look at the intelligence of an insect and of a human and conclude that they are on the same continuum, and that simply making enough small steps would eventually create an AI of human intelligence, provided that it had the intelligence of an insect to begin with.

If a human is on a distinctly other scale (or continuum) than an insect in terms of intelligence, then making small steps looks like progress, but actually has the same problem of the man climbing towards the moon.

\subsection{Words}
An example of a word that can mean different things, depending on the context, is  \textit{desert}.
Desert has multiple meanings. One, a noun, is a dry area, and the other one, a verb, is abandoning someone or something.

For a machine to understand these words, it has to be able to determine the context, and then to apply the correct usage. This, however, is very tricky to do, since determining context doesn't have strict rules. Additionally, word plays and similar things often make use of the different definitions, making it very difficult for a machine to determine when to use what definition.

\subsection{Machine Translation of Natural Language}
Dreyfus did not believe that accurate machine translation was likely (``[T]he ability to learn a language presupposes a complex combination of the uniquely human forms of information processing'') \parencite[32-37]{dreyfus1965alchemy}.

He gave a couple of reasons for this. One is that the subject must be engaged in a form of life in which they share at least some of the goals and interests of the teacher. An AI would usually not have these.

Furthermore, he sees the capacity for natural language as something possible thanks to the ``fringe effect''. Humans are able to reduce ambiguity extremely well, without having to eliminate ambiguity altogether (formalise). Machines, dealing only in distinct ideas, are not able to reduce context-dependent ambiguity in the same manner. Without this effect, Dreyfus does not believe that accurate language translations are possible.

\subsection{Chalmers' Optimism}
Chalmers notes that the idea of a singularity is one that we should take seriously. He recognizes the pattern of over-enthusiasm that Dreyfus' highlights, yet does not take Dreyfus' observation into account when making predictions in the future. Chalmers uses the current successes (in board games, for example) as a predictor for a future singularity.

According to Dreyfus, Chalmers' argument is grounded in the assumption that making small steps will lead to the final goal (singularity). However, as seen in figure \ref{fig:aiclimbstosingularity}, this is a very similar fallacy that Dreyfus introduced before.

\begin{figure}[h]
	\centering
	\includegraphics[width=0.9\linewidth]{media/AITree.png}
	\caption{AI Attempting to reach the Singularity by ``climbing trees''}
	\label{fig:aiclimbstosingularity}
\end{figure}

In figure \ref{fig:aiclimbstosingularity}, there are 4 domains. The ``Current AI'', ``Possible AI via small steps?'', ``Empty space'' and the ``Singularity''.
The Current AI is where AI is now. The next domain is Possible AI via small steps, and is similar to AI researchers ``climbing trees'' to get closer to the Singularity. The empty space is analogous to the space outside of our atmosphere. This is not traversable by ``climbing trees''. Finally, we have the singularity. This might not be reachable at all. If it is, ``climbing trees'' is not the way to reach it, provided that the singularity is on a different continuum than the current state of AI.

\subsection{Personal Thoughts}
\begin{quote}
	\centering
	\textit{These are my personal thoughts on the subject. Don't take them too seriously.}
\end{quote}
Dreyfus' analogy of the man climbing a tree to get to the moon is an excellent example of why making small steps isn't always enough to get to a final goal. If the final goal is on a completely different continuum, something drastic is needed to make the steps to gain the goal.

However, let us take a look at natural selection. In populations, there is some variation in organisms. Some organisms are slightly better than others in some fields and, if the environment ``prefers'' organisms with an expertise in those fields, the better organisms will have a higher chance of passing their DNA (and therefore their expertise) on.

In the case of intelligence, humans have a ancestor who was vastly less intelligent than we are today. This ancestor has had many generations to follow them, 
some of which evolved to become the less intelligent insects. Some descendents also became intelligent humans, however. This process of becoming intelligent did not happen in large steps, but instead happened on some continuum, reaching the ``goal'' of human-level intelligence via many small steps.

Comparing insect-level and human-level intelligence will lead to the conclusion that humans seem on a completely different level. It might even seem impossible to put the two on the same scale. However, I do not see a reason why humans cannot be on the same scale, since humans have become intelligent by \textit{climbing up} this scale. Therefore, if we could create an AI which would have the same broad intelligence of an insect, I do not see a reason why we would eventually (though it might take a long time, perhaps much longer than some optimistic AI researchers suggest) reach broad (or ``general'') intelligence on the same level as humans.

Finally, I believe that Dreyfus was absolutely correct in believing that eliminating ambiguity a process much too expensive to be relied on, a key reason for why he believed translating language was extremely difficult, if not impossible. However, we have seen that current algorithms don't have the same difficulty in determining the context of words as before. IBM's Watson has shown an excellent ``understanding'' (see question \ref{sec:ai.q5}) of the English vocabulary, perhaps showing that the future of AI does not rely on pruning all possible known facts and rules about the language, but that this ``fringe effect'' might be attainable in AI.