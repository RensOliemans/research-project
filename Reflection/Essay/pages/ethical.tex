\section{Ethical Implications}

\lettrine{I}{n} the research being done, a large focus in put on personal data. In today's day and age, new technologies have served us to advance our day-to-day lives, but many of these heralded technologies have also invaded our privacy. Think of sometimes innocuous-sounding technologies such as E-Books and E-Readers \parencite{eff-adobe-privacy}, or rather `obvious' technologies such as mobile phones, which bring a host of invasive problems \parencite{bloomberg-phone}\parencite{eff-nsa-track}.
After these invasions come to public light, regulations are often put in place to limit surveillance (such as \textit{US v Jones} \parencite{eff-us-v-jones} or other cases \parencite{eff-verizon-victory}). An other example of this is of course the \gdpr{}.

Nonetheless, privacy is consistently under threat in the Information Age. The need for privacy, and the value of privacy when weighed against other virtues, is constantly (re-)assessed.

In the case of this study, protecting the personal data of persons via the \whois{} protocol would be the pro-privacy move. Keeping the personal data publicly accessible would be the pro-security move. Additionally, preventing spam, phishing and malware was easier when there was no \gdpr{} present and \whois{} data was publicly accessible, according to \citeauthor{ferrante2018impact} (\citeyear{ferrante2018impact}). In the examples that \citeauthor{ferrante2018impact} gives, he claims that filters against spam and phishing depend on public \whois{} data. However, in the 2 examples that \citeauthor{ferrante2018impact} gives, \whois{} data was not actually the method used, but \textit{could have been} used in order to get the same results as the methods that were actually used.

This distinction is in my opinion an important one. Limiting the public amount of data available on \whois{} would not impact the anti-abuse methods used in \citeauthor{ferrante2018impact}'s examples.

\subsection{Benefit of storing WHOIS Data}
Public \whois{} data could be used to gather information in an investigation about a certain possible threat. This data included email addresses and names, which were used to look at domains which might be used for phishing or malware.

A possible example of this could be a malicious person who registered multiple domains under the same name or email address. When one of these domains is detected as malicious, the other domains can be flagged as possibly malicious as well.

Limiting the availability of \whois{} data could mean that these operations can be more difficult to perform. DomainTools is a company focused on cybersecurity and \whois{}\src{https://domaintools.com}. To quote the response of DomainTools after the \gdpr{}:

\begin{displaycquote}{domaintools-gdpr}
	In May 2018, the General Data Protection Regulation (GDPR) went into effect and subsequently altered the way that cybersecurity analysts are able to perform their investigations. Where, previously, connections between incidents and attribution had often been tied to publicly available \whois{} data, there was now the word REDACTED hindering those linkages.
	
	However, not all was lost with the implementation of GDPR and there are still numerous methods by which to conduct an incident investigation.
\end{displaycquote}

\subsection{Risks of storing WHOIS Data}
The gathering and showing of \whois{} data is in many cases a privacy violation. A registrant registers a domain name and by doing so often has to give some contact details or other personal information. ICANN\src{https://icann.org} requires that the information is correct and up-to-date, so the registrant is not allowed to enter false information when registering a domain \parencite{icann-accurate-whois-data}.

This data can often be retrieved by everyone with an internet connection. The command-line tool \texttt{whois}\src{https://linux.die.net/man/1/whois} is extremely easy to use and, depending on the TLD, personal information about the registrant of a domain is less than a couple of seconds away.

Showing (and even storing) personal information about a registrant can be a violation of one's privacy. An obvious example is:\\

A person who registered the domain name pointing to a forum (or other website) containing views that are not in line with the views of a powerful entity.
This entity is then easily able to request data about that website, leading them to the name, address and phone number of that person.

This powerful entity can be a government or large group, but it can also be a single person who might be able to threaten the registrant now that they have their personal information.
Additionally, they can use their (email) address or phone number for spam, or attempt identity theft.


\subsection{Effects of Privacy Violations}
\begin{displaycquote}{lynch2016internet}
	Partly for these reasons, writers like Jeremy Rifkin have been saying that information privacy is a worn-out idea. [...] So no wonder, the thought goes, we are willing to trade it away - not only for security, but for the increased freedom that comes with convenience.
\end{displaycquote}
In the quote given by Michael Lynch another reference is made to a common thought that privacy is willingly traded away for security and convenience. However, Lynch gives attention to a phenomenon which happens when one's privacy is violated. When one person knows a lot about someone else, they can take advantage of that person. This is especially true when the person whose privacy is violated \textit{is not aware} of the violation. The person doing the violating can now exploit the other.

Lynch also talks about autonomous decisions \parencite[108]{lynch2016internet}, showing that the autonomy of decision is part of what it is to be a fully mature (and individual) person. Privacy matters, at least partly, because information privacy is linked to autonomy. Thereby, it is also an important feature of personhood itself.

As for \whois{} data, some people might not be aware that their personal information is publicly available for anyone with an internet connection. Where Lynch gives examples of a single (though large) entity having access to personal data, in this case everyone can possibly have access to personal data. The advantage here lies with the fact that a person is always able to know \textit{what} information about them is visible. However, not everyone will have the knowledge to do so, leading to undermining and thus loss of autonomy.

This effect is more serious than the advantages that keeping \whois{} data public gives, in my opinion. The advantages are few (even according to DomainTools, numerous investigative methods still exist) yet personal information (in form of full name, (email) address and phone number) of many people is up for grabs by whoever is interested.

\subsection{Summarising}
We have seen a paper describing the effects of public \whois{} data. They gave possible examples where \whois{} data might have been beneficial for a malicious attack by someone \parencite{ferrante2018impact}.
However, in the examples that Ferrante gives, the \whois{} data itself was not actually used - we have yet to see a well-documented example of \whois{} data being actually used for some good.

Additionally, DomainTools is a company focused on cybersecurity and domain data, possibly including \whois{} is an important asset they use. They claim that while the \gdpr{} might hinder the prevention of cybercrime, there are still numerous methods to use.
Something can be said for keeping \whois{} data public, as it might be an asset in some investigations.

On the other hand, this advantage has the cost of storing and showing personal data of a lot of people. Everyone who registers a domain by a TLD in a country not compliant with the \gdpr{} has information which can be found online. This is a serious privacy violation, also since people might not be aware of the (amount of) personal information they have online for everyone to see.

All in all, I claim that the infringement of privacy has a higher weight than the security benefits it provides. Therefore, the implementation of the \gdpr{} -- at least in the scope of \whois{} data -- is a good one, in my opinion.
























