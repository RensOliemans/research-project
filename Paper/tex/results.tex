\section{Results}
The results will first cover the first research question, the fixing of \texttt{pywhois}. Then, the results belonging to personal data will be shown. A comparison between different countries -- EU and Non-EU -- will be made. Finally, Table \ref{tab:ratelimit} shows the Rate Limits encountered during the research.

\subsection{Fixing Pywhois}\label{sec:results-fixing-pywhois}
As explained above, \texttt{pywhois} was not sufficient for the parsing of \whois{} data. Therefore, some changes needed to be made for the library to improve its accuracy.
In total, support for parsing of 3 \tld{}s were completely added to parse in whois: \israel{}, \slovenia{} and \norway{}. The United Arab Emirates \tld{} \texttt{.ae} was also added, but this had no effect in the final results since there were not enough domains for the \tld{} to be added.
In addition, 15 \tld{}s were improved. The library supported parsing for these \tld{}s but this was either incorrect or incomplete. These improved \tld{}s also included \tld{}s such as \texttt{.hr} or \texttt{.cz}, so also included \tld{}s that were not in the final dataset.

The following two paragraphs contain examples of the changes made to improve the \texttt{pywhois} library. These two changes take different approaches and are more or less representative for the rest of the changes.

\paragraph*{Fixing .nl}
Since \netherlands{} has a thick \whois{} model, making a fix for \netherlands{} domains would have a large difference since the fix would apply for all \netherlands{} domains.
Before, the library only parsed the domain name, status, and the name of the registrar.

After the fix, it also parsed the address, country and the DNSSEC \parencite{icann-dnssec} status of the domain.
The output of \netherlands{} \whois{} data was likely changed recently since the names of the registrar fields that \texttt{pywhois} expected were wrong.

\paragraph*{Fixing .dk}
The Denmark \whois{} server does not provide any registrant data by default. However, when one adds \texttt{---show-handles} to the request the registrant data is also shown\src{https://github.com/DK-Hostmaster/whois-service-specification\#request-4}. This was fixed by prepending \texttt{---show-handles} to the request that \texttt{pywhois} makes when it connects with the Danish NIC Client.

\subsection{Personal Data}\label{sec:results-personal-data}
Concerning research question \rqPersonalData{},
Table \ref{tab:personaldata} shows the amount of personal data visible depending on the \tld{}. The column ``\tld{}'' refers to the \tld{} to which the domains belong. ``\# of domains'' means the number of domains that are present of that \tld{} in the dataset used. Finally, ``Personal Data'' refers to the percentage of domains which contained some form of personal data, as defined in section \nameref{sec:measuring}.

Of the 13 \tld{}s in the EU, 3 contained personal information. \denmark{} -- belonging to Denmark -- had full names in the ``Registrant'' fields. However, many used a privacy service to mask the actual full name. The Italian \italy{} domains contained the most personal information of EU domains. This personal information was found in the ``Registrant'' fields, but also in the Admin and Tech fields (``name'', ``address'', ``phone'', ``email''). Finally, Finnish domains (\finland{}) occasionally contained personal information as well. This included their full name and usually the address and phone number.

Of the 7 \tld{}s outside the EU, 5 contained personal information. The Canadian \canada{} domains have redacted registrant fields. However, personal information can still be found in the ``Admin Name'' and ``Admin email'' fields. For both the Israeli (\israel{}) and the Switzerland (\switzerland{}) domains, personal data was not filtered at all. The full names and addresses were visible in all domains that were not owned by a company. In the case of a company, the registrant name could be ``Domain Admin'', for example. In the Israeli domains, the phone numbers were also available.
The US domains often had ``REDACTED FOR PRIVACY'' as a replacement for registrant fields. Still, a combination of email address, phone numbers and physical address was often available, either in the Registrant or in the Admin or Tech fields. As for the Korean domains, they were mostly owned by companies and had little personal information.

A large difference can be seen between \tld{}s belonging to a country within the EU and outside the EU. Taking an average between \tld{}s is slightly complex. If you would do it naively -- 153 \netherlands{} domains with 0\%, and 11 \finland{} domains with 18\% -- you would get an average of \numprint{0.11}\% of domains containing personal data for \netherlands{} and \finland{} domains. This would mean that the \netherlands{} data is vastly overrepresented compared to the \finland{} data.
However, as explained in section \ref{sec:measuring}, it is reasonable to assume that 153 \finland{} domains would have similar results as 11 domains. Therefore, the average of \netherlands{} and \finland{} domains was taken as $ \frac{0\% + 11\%}{2} = 9 $\%.

Via this method, EU domains had personal data on average on \numprint{6.89}\% with a standard deviation of \numprint{0.143}. The non-EU countries had an average of \numprint{36.40}\% and had a standard deviation of \numprint{0.323}. Since the data was distributed non-normally, a standard T-Test is not possible. Instead, a Mann-Whitney \emph{U} test is done. The Mann-Whitney \emph{U} test is a statistical test assessing whether two sampled groups are from a single population \parencite{mcknight2010mann}.

The hypothesis is that EU and Non-EU domains are from statistically different populations, concerning the amount of personal data. An additional hypothesis is that the Non-EU domains have a higher percentage of personal data, so we can use a single-sided \emph{U} test. With the current samples, the U-value is 19. For a significance level of $ p < .05 $, the critical value of $ U $ is 24. Therefore, the result is significant at $ p < .05 $.
So, Non-EU countries have significantly more personal data on average than EU countries.

\begin{table}
	\caption[Personal Data]{Different domains and the percentage of domains containing personal data via \whois{}}
	\label{tab:personaldata}
	\centering
	\begin{tabular}{lrr}
		\toprule
		\tld{}                     & \# of domains & Personal Data \\ \midrule
		EU                         &               &               \\
		\cmidrule{1-1}\texttt{.dk} &            47 &          26\% \\
		\texttt{.ee}               &            10 &           0\% \\
		\texttt{.eu}               &            62 &           0\% \\
		\texttt{.fi}               &            11 &          18\% \\
		\texttt{.fr}               &            13 &           0\% \\
		\texttt{.ie}               &            14 &           0\% \\
		\texttt{.it}               &            74 &          46\% \\
		\texttt{.nl}               &           153 &           0\% \\
		\texttt{.no}               &            31 &           0\% \\
		\texttt{.se}               &            41 &           0\% \\
		\texttt{.si}               &            10 &           0\% \\
		\texttt{.sk}               &            14 &           0\% \\
		\texttt{.uk}               &           146 &           0\% \\
		                           &               &               \\
		Non-EU                     &               &               \\
		\cmidrule{1-1}\texttt{.ca} &            66 &          21\% \\
		\texttt{.ch}               &            13 &          77\% \\
		\texttt{.co}               &            19 &           0\% \\
		\texttt{.il}               &            13 &          62\% \\
		\texttt{.in}               &            48 &           0\% \\
		\texttt{.kr}               &            41 &          27\% \\
		\texttt{.us}               &            41 &          68\% \\ \bottomrule
	\end{tabular}
\end{table}

\subsection{Rate Limits}
Table \ref{tab:ratelimit} shows the different rate limits enforced depending on the Top-Level Domain (or more precisely, the \whois{} server used of the \tld{}. For example, for \texttt{.nl}, the \whois{} server to communicate with is \texttt{whois.domain-registry.nl}).

The ``Number'' column refers to the number of requests that need to be made before being rate-limited.
The ``Duration'' column is relevant when the type of limit is a slowing of the request and means the number of seconds by which the request is slowed.
Finally the ``Cool-Down'' column is the time needed to wait after being rate-limited to ``reset'' the limit.

\begin{table*}
	\caption[Rate Limits]{Different domains and possible rate-limit methods their \whois{} server uses.}
	\label{tab:ratelimit}
	\centering
	\begin{tabular}{lrrrrr}
		\toprule
		\tld{}                     & Type Limit & Number & Duration & Cool-Down &                                                                            Notes \\ \midrule
		EU                         &            &        &          &           &                                                                                  \\
		\cmidrule{1-1}\texttt{.dk} &       Slow &      0 &       2s &         - &                                                  Flat 2s delay for every request \\
		\texttt{.ee}               &       Slow &      5 &      ~8s &        1m &                                                                                  \\
		\texttt{.eu}               &        Ban &     60 &          &        1m &                                                                                  \\
		\texttt{.fi}               &       None &      - &        - &         - &                                                                                  \\
		\texttt{.fr}               &        Ban &      2 &          &       10s &                                                                                  \\
		\texttt{.ie}               &        Ban &    100 &          &       24h &                                                                                  \\
		\texttt{.it}               &       Slow &     20 &      2+s &        1m &                 After 20 request, start slowing requests. Delay keeps increasing \\
		\texttt{.nl}               &        Ban &      1 &          &        1s &                                                  Maximum of 1 request per second \\
		\texttt{.no}               &        Ban &    100 &          &        1m &                                                                                  \\
		\texttt{.se}               &       None &      - &        - &         - &                                                                                  \\
		\texttt{.si}               &        Ban &     10 &          &        1m &                                                                                  \\
		\texttt{.sk}               &        Ban &     50 &          &        1h &                                                                                  \\
		\texttt{.uk}               &        Ban &     25 &          &        5s & Bans large volume requests (\textasciitilde25), lifts ban after slowing requests \\
		                           &            &        &          &           &                                                                                  \\
		Non-EU                     &            &        &          &           &                                                                                  \\
		\cmidrule{1-1}\texttt{.ca} &       None &      - &        - &         - &                                                                                  \\
		\texttt{.ch}               &        Ban &     40 &          &       10m &                                                                                  \\
		\texttt{.co}               &        Ban &    100 &          &        1h &                            \texttt{.co}, \texttt{.in} and \texttt{.us} cooperate \\
		\texttt{.il}               &        Ban &     30 &          &       10m &                                                                                  \\
		\texttt{.in}               &        Ban &    100 &          &        1h &                            \texttt{.co}, \texttt{.in} and \texttt{.us} cooperate \\
		\texttt{.kr}               &       None &      - &        - &         - &                                                                                  \\
		\texttt{.us}               &        Ban &    100 &          &        1h &                            \texttt{.co}, \texttt{.in} and \texttt{.us} cooperate \\ \bottomrule
		                           &            &        &          &           &
	\end{tabular}
\end{table*}