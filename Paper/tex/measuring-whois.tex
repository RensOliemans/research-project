\section{Measuring Data}\label{sec:measuring}
This section contains the methodology of the research: how the results were measured and computed.
The first part concerns how Research Question \ref{rq:parsing} will be answered, so how the current state of the art can be improved. This section will also cover the ways the data was gathered. Determining personal data is the last part of this section, related to the last three Research Questions.

% Fixing pywhois
The first Research Question reads \rqParsing{}.\\
There are many ways to obtain \whois{} data, as seen in section \ref{sec:related-work}. For this study, the \texttt{pywhois} library was used, as it was the best viable option available which was open to use.
However, the library was still not accurate enough, especially when parsing Top-Level Domains that were not common worldwide (such as \texttt{.nl}, \texttt{.se}, etc.). In order to properly parse \whois{} data and analyse it for personal data, the library had to be fixed. Since there was limited time, the improvements made are the ones which would have the largest impact given the time put in. Support for some top-level domains had to be added completely, but most fixes of \texttt{pywhois} were fixes that were small to implement, yet gained a large benefit to the working of the library.
Some examples of fixes are shown in section \ref{sec:results-fixing-pywhois}.

% Defining personal data
The second Research Question (\rqPersonalData{}) is concerned with personal data. In order to be able to answer it, a definition of personal data must first be given. The EU defines it as follows:
\begin{displaycquote}{gdpr-eu}
	Personal Data is any information that relates to an identified or identifiable living individual. Different pieces of information, which collected together can lead to the identification of a particular person, also constitute personal data.
\end{displaycquote}

\begin{figure}
	\centering
	\includegraphics[width=\linewidth]{media/whois_example.png}
	\caption{An example of some WHOIS output from a Danish domain. The personal data has been blurred to protect the person's privacy.}
	\label{fig:whois-example}
\end{figure}

To better understand how personal data might exist in \whois{} data, an example of a typical \whois{} query output has been given in figure \ref{fig:whois-example}.
In the case of \whois{}, personal data is only present in the \textit{registrant} fields, as the registrant is the person registering the website.
The \textit{registrar} fields, on the other hand, contain data about registrars,  which are always large companies. So, they contain no personal data about the people having registered the domain.

% Data
For the second, third and fourth Research Questions this research needs to obtain a large amount of \whois{} data.
The data used for this research comes from random domains published by OpenDNS \cite{opendns2018}.
All \tld{}s with 10 or more domains were looked at in this research. In total, there were 19 of such \tld{}s, with a median of 30 domains per \tld{}.\\
Comparing multiple datasets with 10 elements is usually not representative. In \whois{} it is a bit different. The top-level registries investigated in this research all use a \textit{thick} \whois{} lookup model. Thus, the structure of \whois{} data is the same for all domains within that \tld{}. This means that there is little to no variation between domains.\\
Additionally, the top-level registry determines the rules of the \whois{} data. So, a top-level registry -- for the Netherlands a top-level registry would be SIDN -- enforces the same rules for all registrars. So, when some personal information is available in some domains in the \tld{}, it is reasonable to suppose that personal information is present in many domains for that \tld{}.

Finally, the data has to be analysed and personal data needs to be detected. A definition of personal data was already given, but finding it is still not trivial. Determining personal data automatically remains a difficulty. One can look into the ``name'', email'', ``address'' or ``phone'' fields of the registrants, but, whereas it is easy to see that \texttt{REDACTED\_FOR\_PRIVACY} is not a personal name, it can be difficult to assess whether a name belongs to a person or company.
Also, the given names are sometimes ``Domain Administrator'', ``<company> Support'', ``Web Master'', etcetera. These are clearly no personal names, but can be difficult to automatically detect.

Therefore, the data found was parsed automatically via \texttt{pywhois}, but inspected manually to see if it contained personal information.