\section{Introduction}\label{sec:introduction}
% Introduction to domain names
Domain names are an essential part of the internet. Domain Name Servers map a domain name, like \url{https://duckduckgo.com}, to an IP address. This allows you to browse to that domain name instead of having to go to the IP address belonging to the server.
When registering a domain name, one has to give some information to the \textit{registrar} -- a company which is responsible for controlling domain names. This usually includes personal information such as their full name, email addresses, phone numbers and physical addresses.

In many cases, this personal information is available for everyone with an internet connection, making it very easy for someone's personal information to be seen. This information about a domain name -- and the person who registered it -- can be accessed via the \whois{} protocol. An example of such a query can be seen in figure \ref{fig:whois-query}.

\begin{figure}
	\centering
	\includegraphics[width=\linewidth]{media/whoisquery.png}
	\caption{Overview of a WHOIS Query}
	\label{fig:whois-query}
\end{figure}

% Introduction to gdpr
The \gdpr{} is a regulation about the collection and protection of personal data, introduced in May 2018 \parencite{gdpr-eu}. 
\whois{}, on the other hand, is a way of gathering (often personal) data about the registrants.
Since the \gdpr{}, registrars are not allowed to publicly show this data of EU citizens any more. This had lead to a couple of countries restricting the amount of personal data visible on domains within their Top-Level Domain. A Top-Level Domain, or \tld{}, is a domain at the highest hierarchy. For example, for the domain name \url{www.example.com}, the \tld{} is \texttt{.com}.
Top-level registries handle the way \whois{} data is returned. For the Netherlands (\netherlands{}), the organisation doing so is the SIDN. The SIDN restricted personal data via \whois{} in March 2018 \parencite{sidn2018gdpr}.

As of now, the \gdpr{} has been in effect for over a year. Yet, no recent assessment of public personal information via the \whois{} protocol exists. Some countries have claimed they restricted the visible personal data \parencites{sidn2018gdpr}{norid2018gdpr} but not all EU countries have done so.

All registrars which contain personal information about EU citizens have to comply with the \gdpr{}. However, it is unclear how many registrars actually do so.
This research will show the amount of personal information available on different \tld{}s, within an outside the European Union. 
The results of this research might be used for these registrars -- or top-level registries such as SIDN -- to see how much personal data is still -- unlawfully -- public via the \whois{} protocol.

% Introduction to this study
In order to measure the amount of personal information available via \whois{}, a large amount of \whois{} data has to be parsed first.
Parsing of \whois{} is notoriously difficult \parencite{liu2015learning}. Nearly all Top-Level Domains -- \tld{}s, such as \netherlands{} -- have a custom structure for outputting \whois{} data.
One of the most active open source tools to parse this \whois{} data is \texttt{pywhois}\src{https://pypi.org/project/python-whois}. This library parses \whois{} data, but is not very accurate for many domains. In order to analyse \whois{} data, this library must first be improved for some \tld{}s.

% Research questions
To summarise, there are no easy ways to accurately parse \whois{} data on a large scale as of now. Additionally, it is unknown how much personal information is available via \whois{}, while showing this data is incompliant with the \gdpr{}. This creates some interesting research questions:

\begin{rqs}
	\item \rqParsing{}\label{rq:parsing}
	\item \rqPersonalData{}\label{rq:personal-data}
	\item \rqEUvsNonEU{}\label{rq:eu-vs-noneu}
	\item \rqEUvsEU{}\label{rq:eu-vs-eu}
\end{rqs}

% Structure and approach of the study
To answer these questions, \whois{} data must first be parsed. However, in order to sufficiently say something about the parsed data, one needs to be sure that the data is accurately parsed. In \whois{}, two different lookup models exist: \textit{Thick} and \textit{thin} \parencite{icann-thick-thin}.
The models refer to the way labelling and displaying of \whois{} output is done. In thick models, the format of \whois{} is the same for all domains within that \tld{}. In thin models, however, the domains are not required to have a certain format. This leads to the registrars within a \tld{} themselves determining how the format should be \parencite{icann-thick-thin}, making parsing thin lookup models quite difficult due to the different formats.
Therefore, this research will only parse domains within a \tld{} that has a thick lookup model.

First, the parsing of \texttt{pywhois} will be improved. This question is answered in section \ref{sec:discussion-improving-parsing}.

Secondly, the amount of personal data will be analysed. This question will be answered by filtering personal information from \whois{} data, as seen in section \ref{sec:discussion-results}.
Finally, the last two questions will be answered via statistical measures in section \ref{sec:discussion-summary}.